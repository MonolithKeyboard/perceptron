﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lr2
{
    public class Neuron
    {
        public double[,] Weights { get; private set; }
        public int[,] Input { get; private set; }

        public Neuron()
        {
            Weights = new double[30,30];
            Random rnd = new Random();
            for (int i = 0; i < Weights.GetLength(0); i++)
            {
                for (int j = 0; j < Weights.GetLength(1); j++)
                {
                    Weights[i, j] = rnd.NextDouble()*2-1;
                }
            }
        }
        public void SetWeights(double[,] weights)
        {
            Weights = weights;
        }
        public void SetInput(int[,] input)
        {
            Input = input;
        }
        public int ActivationFunc()
        {
            double potential = CalculatePotential();
            if (potential >= 0)
            {
                return 1;
            }
            else return 0;
        }
        private double CalculatePotential()
        {
            double potential = 0;
            for (int i = 0; i < Weights.GetLength(0); i++)
            {
                for (int j = 0; j < Weights.GetLength(1); j++)
                {
                    potential += Weights[i, j] * Input[i, j];
                }
            }
            return potential;
        }
    }
}
