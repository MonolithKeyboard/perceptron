﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lr2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        
        private List<int[]> etalonList = Utilities.GenerateEtalonVetctorList();
        private int[] currentEtalon;
        private int[] output = new int[33];
        private double learningSpeed = 0.1;
        private int[,] inputImage;
        private List<Neuron> neuronLayer = new List<Neuron>();
        private void Form1_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < 33; i++)
            {
                neuronLayer.Add(new Neuron());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string filePath = "";
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "png files (*.png)|*.png|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    filePath = openFileDialog.FileName;
                }
            }
            filenameRichTextBox.Text += filePath;
            Image letterImage = Image.FromFile(filePath);
            Bitmap bitmap = new Bitmap(letterImage);
            int[,] input = new int[30, 30];
            for (int i = 0; i < input.GetLength(0); i++)
            {
                for (int j = 0; j < input.GetLength(1); j++)
                {
                    Color c = bitmap.GetPixel(i, j);
                    if (c == Color.FromArgb(0, 0, 0)) 
                    {
                        input[i, j] = 1;
                    }
                }
            }
            inputImage = input;
            filenameRichTextBox.Text += "\n";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 33; i++)
            {
                Neuron currentNeuron = neuronLayer.ElementAt(i);
                currentNeuron.SetInput(inputImage); 
                output[i] = currentNeuron.ActivationFunc();
                if (output[i] == 1) resultRichTextBox.Text += Utilities.Alphabet[i];
                else resultRichTextBox.Text += "0";
            }
            resultRichTextBox.Text += "\n";
        }
        private void RecalculateWeights()
        {
            ValidateEtalonAndImage();
            for (int n = 0; n < 33; n++)
            {
                int[] error = CalculateError(output);
                double[,] weightToRecalc = neuronLayer.ElementAt(n).Weights;
                int[,] input = neuronLayer.ElementAt(n).Input;
                int thisNeuronError = error[n];
                double[,] newWeights = new double[30, 30];
                for (int i = 0; i < 30; i++)
                {
                    for (int j = 0; j < 30; j++)
                    {
                        newWeights[i, j] = weightToRecalc[i, j] +
                            learningSpeed * thisNeuronError * input[i, j];
                    }
                }
                neuronLayer.ElementAt(n).SetWeights(newWeights);
            }
        }
        private void ValidateEtalonAndImage()
        {
            if (etalonList == null)
            {
                MessageBox.Show("Выберите эталон");
                return;
            }
            if (inputImage == null)
            {
                MessageBox.Show("Загрузите изображение");
                return;
            }
        }
        private int[] CalculateError(int[] output)
        {
            int[] error = new int[33];
            for (int i = 0; i <  output.Length; i++)
            {
                error[i] = currentEtalon[i] - output[i];
            }
            return error;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            RecalculateWeights();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < 33; i++)
            {
                if (comboBox1.Text == Utilities.Alphabet[i].ToString())
                {
                    currentEtalon = etalonList.ElementAt(i);
                }
            }
        }
    }
}
