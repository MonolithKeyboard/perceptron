﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lr2
{
    internal class Utilities
    {
        public static char[] Alphabet = new char[]
        {
            'a','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р',
            'с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я'
        };
        public static List<int[]> GenerateEtalonVetctorList()
        {
            List<int[]> etalonList = new List<int[]>();
            for (int i = 0; i < 33; i++)
            {
                int[] etalonVector = new int[33];
                etalonVector[i] = 1;
                etalonList.Add(etalonVector);
            }
            return etalonList;
        }
    }
}
